//! Invoke the dark power of Zalgo in your Rust code.

use {
    proc_macro2::{TokenStream, TokenTree},
    quote::quote,
    rand::SeedableRng,
    rand_xorshift::XorShiftRng,
    std::collections::HashMap,
    syn::{
        parse_macro_input,
        visit_mut::{self, VisitMut},
        Ident, Item, Macro,
    },
};

mod zalgo;

const SEED: [u8; 16] = [
    0x5A, 0x16, 0x05, 0xA6, 0x10, 0x5A, 0x16, 0x05, 0xA6, 0x10, 0x5A, 0x16, 0x05, 0xA6, 0x10, 0xD,
];

const PREFIX: &str = "__pure_";

struct Visitor {
    cache: HashMap<String, String>,
    rng: XorShiftRng,
}

impl VisitMut for Visitor {
    fn visit_ident_mut(&mut self, ident: &mut Ident) {
        let sident = ident.to_string();
        if sident.len() > PREFIX.len() && sident.starts_with(PREFIX) {
            // Strip prefixes of prefixed idents.
            *ident = Ident::new(&sident[PREFIX.len()..], ident.span());
        } else if let Some(zident) = self.cache.get(&sident) {
            // Use already-generated idents.
            *ident = Ident::new(zident, ident.span());
        } else {
            // Generate a new ident.
            let mut zident = String::new();
            zalgo::generate(&mut self.rng, &sident, &mut zident);
            *ident = Ident::new(&zident, ident.span());
            self.cache.insert(sident, zident);
        }
    }

    fn visit_macro_mut(&mut self, mac: &mut Macro) {
        visit_mut::visit_macro_mut(self, mac);

        let mut output = TokenStream::new();
        for token in mac.tokens.clone() {
            match token {
                TokenTree::Ident(mut ident) => {
                    // Handle idents in macro invocations.
                    self.visit_ident_mut(&mut ident);
                    output.extend(Some(TokenTree::Ident(ident)));
                }
                _ => {
                    output.extend(Some(token));
                }
            }
        }
        mac.tokens = output;
    }
}

// TODO: configuration
/// Zalgo-ify identifiers.
///
/// # Behavior
///
/// The produced identifiers are random but deterministic.
/// However, they may differ between invocations.
///
/// # Example
///
/// ```
/// # #![allow(uncommon_codepoints)]
/// # use zalgoify::zalgoify;
/// #[zalgoify]
/// fn cast(x: __pure_u32) -> __pure_i32 {
///     // Identifiers may be preserved with the `__pure_` prefix.
///     unsafe { __pure_std::__pure_mem::__pure_transmute(x) }
/// }
/// ```
#[proc_macro_attribute]
pub fn zalgoify(
    _attr: proc_macro::TokenStream,
    item: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let mut item = parse_macro_input!(item as Item);
    let mut visitor = Visitor {
        cache: HashMap::new(),
        rng: XorShiftRng::from_seed(SEED),
    };
    visitor.visit_item_mut(&mut item);
    quote!(#item).into()
}
