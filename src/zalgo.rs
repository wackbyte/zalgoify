use rand::Rng;

pub const ZALGO_UP: [char; 50] = [
    '̍', '̎', '̄', '̅', '̿', '̑', '̆', '̐', '͒', '͗', '͑', '̇', '̈', '̊', '͂', '̓', '̈́', '͊', '͋', '͌', '̃', '̂', '̌', '͐',
    '̀', '́', '̋', '̏', '̒', '̓', '̔', '̽', '̉', 'ͣ', 'ͤ', 'ͥ', 'ͦ', 'ͧ', 'ͨ', 'ͩ', 'ͪ', 'ͫ', 'ͬ', 'ͭ', 'ͮ', 'ͯ', '̾', '͛',
    '͆', '̚',
];

pub const ZALGO_MIDDLE: [char; 22] = [
    '̕', '̛', '̀', '́', '͘', '̡', '̢', '̧', '̨', '̴', '̵', '̶', '͏', '͜', '͝', '͞', '͟', '͠', '͢', '̸', '̷', '͡',
];

pub const ZALGO_DOWN: [char; 40] = [
    '̖', '̗', '̘', '̙', '̜', '̝', '̞', '̟', '̠', '̤', '̥', '̦', '̩', '̪', '̫', '̬', '̭', '̮', '̯', '̰', '̱', '̲', '̳', '̹',
    '̺', '̻', '̼', 'ͅ', '͇', '͈', '͉', '͍', '͎', '͓', '͔', '͕', '͖', '͙', '͚', '̣',
];

fn is_zalgo(c: char) -> bool {
    ZALGO_UP.contains(&c) || ZALGO_MIDDLE.contains(&c) || ZALGO_DOWN.contains(&c)
}

fn gen_item<'a, T, R>(rng: &mut R, items: &'a [T]) -> &'a T
where
    R: ?Sized + Rng,
{
    &items[rng.gen_range(0..items.len())]
}

// TODO: configuration
pub fn generate<R>(rng: &mut R, input: &str, output: &mut String)
where
    R: ?Sized + Rng,
{
    for c in input.chars() {
        output.push(c);

        // Don't zalgoify already-zalgo characters.
        if !is_zalgo(c) {
            if rng.gen() {
                output.push(*gen_item(rng, &ZALGO_UP));
            }

            if rng.gen() {
                output.push(*gen_item(rng, &ZALGO_MIDDLE));
            }

            if rng.gen() {
                output.push(*gen_item(rng, &ZALGO_DOWN));
            }
        }
    }
}
