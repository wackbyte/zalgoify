#![allow(uncommon_codepoints, unused_imports)]

use zalgoify::zalgoify;

#[zalgoify]
fn foo() {
    use __pure_println as println;
    let zalgo = "he comes";
    println!("{}", zalgo);
}

fn main() {
    f̹͛o̒o̸̊();
}
